\NeedsTeXFormat{LaTeX2e}
\PrividesClass{fiasco}[2016/02/16 Fiasco playset class]

\pagestyle{empty}

\RequirePackage[top=.75in,inner=1in,outer=1in,bottom=.75in]{geometry}
\RequirePackage{fontspec}
\RequirePackage{titlesec}
\RequirePackage{xcolor}
\RequirePackage{epsdice}

\setlength{\parskip}{10pt}
\setlength{\parindent}{0pt}

\newcommand{\HText}[1]%
{{\fontsize{24}{24}\fontspec{Hitchcock}\addfontfeature{Color=b71d21}#1}}
\newcommand{\insetting}{\par\HText{...in SETTING}}
\newcommand{\Relationships}{\HText{Relationships...}\par~}
\newcommand{\Needs}{\HText{Needs...}\par~}
\newcommand{\Objects}{\HText{Objects...}\par~}
\newcommand{\Locations}{\HText{Locations...}\par~}
\newcommand{\Thescore}{\par\HText{The Score}\par~}

\newenvironment{Category}[1]%
{\par{\fontsize{16}{16}\fontspec{Hitchcock}#1}%
\begin{enumerate}%
\renewcommand{\labelenumi}{\Large\epsdice[white]{\arabic{enumi}}}}%
{\end{enumerate}}

