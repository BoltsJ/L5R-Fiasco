# A Rokugan fiasco

This is a playset for the Fiasco tabletop RPG using the Rokugan setting from
Legend of the Five Rings.

## [Download](https://gitlab.com/BoltsJ/L5R-Fiasco/raw/master/l5r-fiasco.pdf)

## Building

In order to compile the LaTeX documents, the XeLaTeX compiler is used to allow
TTF fonts through the fontspec package.  For licensing reasons, the fonts are
not included.  Links to download the fonts are provided below.

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a><br />This work is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
Creative Commons Attribution-ShareAlike 4.0 International License</a>.

### Fonts (Required to build)

Hitchcock was created by Matt Terich, based on the work of Saul Bass. Please do
not redistribute these files in any way. They can be downloaded for free at
http://typographica.org/001110.php

Vertigo font from http://fontoville.com/.
http://www.fontoville.com/downloads/Vertigo_wwwFontovilleCom.zip
